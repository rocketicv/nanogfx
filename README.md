# NanoGFX

NanoGFX is a small, capable wrapper around the Canvas API, with no external dependencies.

![GitHub](https://img.shields.io/badge/license-MIT-brightgreen.svg)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Usage

To build NanoGFX:

1) Clone this repository
2) Run `npm install`.
3) Run `npm build`.

This will output the file `dist/nanoGFX.min.js`.

A pre-built download will be available soon.

To use NanoGFX in your project:

1) Import it in your `main.js` file as shown below.
2) Do whatever you want (program logic, data etc.)
3) Use [`webpack`](https://webpack.js.org/) to bundle your project.

Import syntax:

```js
import { NanoGFX } from './nanoGFX.min.js'
```

## Documentation

Coming soon.

## License

NanoGFX is licensed under the MIT license, provided in `LICENSE.txt` and copied below in the "MIT License" sub-heading.

### MIT License

Copyright (c) 2018 error0x404

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
