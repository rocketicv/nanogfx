/**
 * index.js
 */

import { Sprite } from './sprite.js'
import { Display } from './display.js'

export const NanoGFX = {}

NanoGFX.Sprite = Sprite
NanoGFX.Display = Display
