/**
 * display.js
 * Contains all the canvas initialisation and sprite display code for NanoGFX.
 */
'use strict'

export const Display = {}

Display.canvas = null // the actual canvas.
Display.ctx = null

/**
 * Initialise the canvas for use
 * @param {string} canvasID - The ID of the canvas to get.
 * @param {number} width - The width to set the canvas to. If 0, it is left unchanged.
 * @param {number} height - The height to set the canvas to. If 0, it is left unchanged.
 * @param {number} bgcolor - The backkground colour of the canvas, in the format 'rgb(nnn,nnn,nnn)'.
 */
Display.init = function (canvasID, width, height, bgcolor) {
  Display.canvas = document.getElementById('nanoGFXCanvas')
  Display.ctx = Display.canvas.getContext('2D')
}

Display.clearCanvas = function () {
  Display.ctx.clearRect(0, 0, Display.canvas.width, Display.canvas.height)
}
Display.displaySprite = function () {}

Display.setCanvasProperties = function () {}
