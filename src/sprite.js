/**
 * sprite.js
 * Contains classes representing the various types of sprites supported by NanoGFX.
 */
'use strict'

export const Sprite = {}

/** Represents a sprite. */
Sprite.Sprite = class {
  /**
   * Construct a sprite.
   * @param {number} width - The sprite's height.
   * @param {number} height - The sprite's width.
   */
  constructor (width, height) {
    this.width = width
    this.height = height
  }
}

/** Represents a generic shape. */
Sprite.Shape = class extends Sprite.Sprite {
  /**
   * Construct a shape.
   * @param {string} lineColour - The line colour of the shape, in the format 'rgb(nnn,nnn,nnn)'.
   * @param {string} fillColour - The fill colour of the shape, in the format 'rgb(nnn,nnn,nnn)'.
   */
  constructor (lineColour, fillColour) {
    super()
    this.lineColour = lineColour
    this.fillColour = fillColour
  }
}

/** Represents a rectangle. */
Sprite.Rectangle = class extends Sprite.Shape {
  /**
   * Construct a rectangle.
   * @param {number} x1 - The x coordinate of the top-left corner of the rectangle.
   * @param {number} y1 - The y coordinate of the top-left corner of the rectangle.
   * @param {number} x2 - The x coordinate of the bottom-right corner of the rectangle.
   * @param {number} y2 - The y coordinate of the bottom-right corner of the rectangle.
   */
  constructor (x1, y1, x2, y2) {
    super()
    this.x1 = x1
    this.y1 = y1
    this.x2 = x2
    this.y2 = y2
  }
}

/** Represents a triangle. */
Sprite.Triangle = class extends Sprite.Shape {
  /**
   * Construct a triangle.
   * @param {number} x1 - The x coordinate of the first point of the rectangle.
   * @param {number} y1 - The y coordinate of the first point of the rectangle.
   * @param {number} x2 - The x coordinate of the second point of the rectangle.
   * @param {number} y2 - The y coordinate of the second point of the rectangle.
   * @param {number} x3 - The x coordinate of the third point of the rectangle.
   * @param {number} y3 - The y coordinate of the third point of the rectangle.
   */
  constructor (x1, y1, x2, y2, x3, y3) {
    super()
    this.x1 = x1
    this.y1 = y1
    this.x2 = x2
    this.y2 = y2
    this.x3 = x3
    this.y3 = y3
  }
}

/** Represents an ellipse. */
Sprite.Ellipse = class extends Sprite.Shape {
  /**
   * Construct an ellipse.
   * @param {number} x - The x coordinate of the ellipse.
   * @param {number} y - The y coordinate of the ellipse.
   * @param {number} w - The width of the ellipse.
   * @param {number} h - The height of the ellipse.
   */
  constructor (x, y, w, h) {
    super()
    this.x = x
    this.y = y
    this.w = w
    this.h = h
  }
}

/** Represents an arc. Not implemented yet. **/
Sprite.Arc = class extends Sprite.Shape {
  /**
   * Construct an arc. Not implemented yet.
   */
  constructor () {
    super()
    console.log('Arc: Not Implemented')
  }
}

/** Represents an image. Not implemented yet. **/
Sprite.Image = class extends Sprite.Sprite {
  /**
   * Construct an image. Not implemented yet.
   */
  constructor () {
    super()
    console.log('Image: Not Implemented')
  }
}
