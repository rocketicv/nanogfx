const path = require('path')

const config = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'nanoGFX.min.js'
  },
  mode: 'production'
}

module.exports = config
